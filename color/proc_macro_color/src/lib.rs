#![crate_type = "proc-macro"]
extern crate proc_macro;

use proc_macro::{Delimiter, Group, Ident, Literal, Punct, Spacing, Span, TokenStream, TokenTree};
use std::collections::LinkedList;

fn remove_whitespace(dat : String) -> String {
    let (mut out_list, out_size) = {
        let split = dat.split_whitespace();

        let mut out_list = LinkedList::new();
        let mut out_size = 0;

        for i in split {
            if !i.is_empty() {
                out_list.push_back(i);
                out_size += i.len();
            }
        }

        (out_list, out_size)
    };

    let mut ret = String::with_capacity(out_size);

    while let Some(e) = out_list.pop_front() {
        ret.push_str(e);
    }

    ret
}

fn parse_hex(dat : &str) -> Option<[u8; 4]> {
    let dat = dat.trim().trim_matches('\"').trim_start_matches('#');

    match dat.len() {
        3 | 4 | 6 | 8 => {
            let mut out = [0, 0, 0, u8::MAX];

            for (i, c) in dat.char_indices() {
                if let Some(digit) = c.to_digit(16) {
                    let digit = digit as u8;
                    match dat.len() {
                        3 | 4 => {
                            out[i] = digit | digit << 4;
                        },
                        6 | 8 => {
                            out[i / 2] |= digit << if i % 2 == 0 { 4 } else { 0 }
                        },
                        _ => {},
                    }
                } else {
                    return None;
                }
            }

            Some(out)
        },
        _ => None
    }
}

#[proc_macro]
pub fn hex(item : TokenStream) -> TokenStream {
    let raw = remove_whitespace(item.to_string());

    if let Some(parsed) = parse_hex(raw.as_str()) {
        let mut stream = TokenStream::new();
        let mut args_stream = TokenStream::new();
        let mut args_list = LinkedList::new();

        for i in parsed {
            args_list.push_back(TokenTree::Literal(Literal::u8_unsuffixed(i)));
            if !args_list.is_empty() {
                args_list.push_back(TokenTree::Punct(Punct::new(',', Spacing::Alone)));
            }
        }

        args_stream.extend(args_list);

        stream.extend([
            TokenTree::Ident(Ident::new("Color", Span::call_site())),
            TokenTree::Punct(Punct::new(':', Spacing::Joint)),
            TokenTree::Punct(Punct::new(':', Spacing::Joint)),
            TokenTree::Ident(Ident::new("rgba", Span::call_site())),
            TokenTree::Group(Group::new(Delimiter::Parenthesis, args_stream))
        ]);

        stream
    } else {
        panic!("{} must be an hexadecimal value (3F3B6E, #3F3B6E...)", item);
    }
}