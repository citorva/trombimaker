mod error;

use std::collections::{HashMap, LinkedList};
use lazy_static::lazy_static;

#[cfg(feature="serde")]
use serde::{ Serialize, Deserialize, Serializer, Deserializer };
#[cfg(feature="serde")]
use serde::de::Visitor;
#[cfg(feature="serde")]
use std::fmt::Formatter;
#[cfg(feature="schemars")]
use schemars::{ JsonSchema, Map };
#[cfg(feature="schemars")]
use schemars::schema::{Schema, SchemaObject, InstanceType, StringValidation};

pub use proc_macro_color::hex;
pub use error::*;
use crate::Error::{NotEnoughArguments, TooManyArguments};

#[cfg(test)]
mod tests {
    use crate::Color;
    use crate::hex;

    #[test]
    fn test_hexa() {
        assert!(Color::parse_hex("1F4A1D").is_ok());
        assert!(Color::parse_hex("  1F4A1D   ").is_ok());
        assert!(Color::parse_hex("3FA").is_ok());
        assert!(Color::parse_hex("  3FA   ").is_ok());

        assert!(Color::parse_hex("00000").is_err());
        assert!(Color::parse_hex("3FR").is_err());
    }

    #[test]
    fn check_hexa_value() {
        assert!(Color::parse_hex("123").unwrap() == Color::rgb(17, 34, 51));
        assert!(Color::parse_hex("9DF35E").unwrap() == Color::rgb(157, 243, 94));
    }

    #[test]
    fn check_rgb_function() {
        assert!(Color::try_from("rgb(231, 26, 32)").unwrap() == Color::rgb(231, 26, 32));
    }

    #[test]
    fn check_by_name() {
        assert!(Color::try_from("red").unwrap() == Color::rgb(255, 0, 0));
    }

    #[test]
    fn check_macro() {
        assert!(hex!(#F0F8FF) == Color::rgb(240, 248, 255));
        assert!(hex!(# F0F8FF) == Color::rgb(240, 248, 255));
        assert!(hex!(#F0 F8 FF) == Color::rgb(240, 248, 255));
        assert!(hex!(F0F8FF) == Color::rgb(240, 248, 255));
        assert!(hex!(F0 F8 FF) == Color::rgb(240, 248, 255));
    }
}

macro_rules! hashmap {
    ($( $key: expr => $val: expr ),*) => {{
         let mut map = ::std::collections::HashMap::new();
         $(
            map.insert($key, $val);
         )*
         map
    }}
}

lazy_static! {
    static ref COLOR_MAP : HashMap<&'static str, Color> = hashmap![
             "algae green" => hex!("#64E986"),
               "aliceblue" => hex!("#F0F8FF"),
             "alien green" => hex!("#6CC417"),
            "antiquewhite" => hex!("#FAEBD7"),
              "aquamarine" => hex!("#7FFFD4"),
              "army brown" => hex!("#827B60"),
                "ash gray" => hex!("#666362"),
           "avocado green" => hex!("#B2C248"),
           "aztech purple" => hex!("#893BFF"),
                   "azure" => hex!("#F0FFFF"),
               "baby blue" => hex!("#95B9C7"),
            "bashful pink" => hex!("#C25283"),
      "basket ball orange" => hex!("#F88158"),
         "battleship gray" => hex!("#848482"),
                "bean red" => hex!("#F75D59"),
              "bee yellow" => hex!("#E9AB17"),
                    "beer" => hex!("#FBB117"),
            "beetle green" => hex!("#4C787E"),
                   "beige" => hex!("#F5F5DC"),
               "black cat" => hex!("#413839"),
               "black cow" => hex!("#4C4646"),
               "black eel" => hex!("#463E3F"),
                   "black" => hex!("#000000"),
          "blanchedalmond" => hex!("#FFEBCD"),
                  "blonde" => hex!("#FBF6D9"),
               "blood red" => hex!("#7E3517"),
            "blossom pink" => hex!("#F9B7FF"),
              "blue angel" => hex!("#B7CEEC"),
            "blue diamond" => hex!("#4EE2EC"),
              "blue dress" => hex!("#157DEC"),
               "blue eyes" => hex!("#1569C7"),
               "blue gray" => hex!("#98AFC7"),
              "blue hosta" => hex!("#77BFC7"),
                "blue ivy" => hex!("#3090C7"),
                "blue jay" => hex!("#2B547E"),
                "blue koi" => hex!("#659EC7"),
             "blue lagoon" => hex!("#8EEBEC"),
              "blue lotus" => hex!("#6960EC"),
             "blue orchid" => hex!("#1F45FC"),
             "blue ribbon" => hex!("#306EFF"),
              "blue whale" => hex!("#342D7E"),
             "blue zircon" => hex!("#57FEFF"),
              "blue green" => hex!("#7BCCB5"),
          "blueberry blue" => hex!("#0041C2"),
              "blush pink" => hex!("#E6A9EC"),
               "blush red" => hex!("#E56E94"),
                   "brass" => hex!("#B5A642"),
             "bright gold" => hex!("#FDD017"),
        "bright neon pink" => hex!("#F433FF"),
                  "bronze" => hex!("#CD7F32"),
              "brown bear" => hex!("#835C3B"),
             "brown sugar" => hex!("#E2A76F"),
            "bullet shell" => hex!("#AF9B60"),
                "burgundy" => hex!("#8C001A"),
               "burlywood" => hex!("#DEB887"),
              "burnt pink" => hex!("#C12267"),
          "butterfly blue" => hex!("#38ACEC"),
           "cadillac pink" => hex!("#E38AAE"),
             "camel brown" => hex!("#C19A6B"),
        "camouflage green" => hex!("#78866B"),
              "cantaloupe" => hex!("#FFA62F"),
                 "caramel" => hex!("#C68E17"),
             "carbon gray" => hex!("#625D5D"),
          "carnation pink" => hex!("#F778A1"),
                 "celeste" => hex!("#50EBEC"),
               "champagne" => hex!("#F7E7CE"),
                "charcoal" => hex!("#34282C"),
              "chartreuse" => hex!("#8AFB17"),
              "cherry red" => hex!("#C24641"),
            "chestnut red" => hex!("#C34A2C"),
                "chestnut" => hex!("#954535"),
           "chilli pepper" => hex!("#C11B17"),
               "chocolate" => hex!("#C85A17"),
                "cinnamon" => hex!("#C58917"),
             "cloudy gray" => hex!("#6D6968"),
            "clover green" => hex!("#3EA055"),
             "cobalt blue" => hex!("#0020C2"),
                  "coffee" => hex!("#6F4E37"),
           "columbia blue" => hex!("#87AFC7"),
"construction cone orange" => hex!("#F87431"),
            "cookie brown" => hex!("#C7A317"),
                  "copper" => hex!("#B87333"),
              "coral blue" => hex!("#AFDCEC"),
                   "coral" => hex!("#FF7F50"),
             "corn yellow" => hex!("#FFF380"),
         "cornflower blue" => hex!("#6495ED"),
                "cornsilk" => hex!("#FFF8DC"),
            "cotton candy" => hex!("#FCDFFF"),
               "cranberry" => hex!("#9F000F"),
                   "cream" => hex!("#FFFFCC"),
                 "crimson" => hex!("#E238EC"),
           "crocus purple" => hex!("#9172EC"),
            "crystal blue" => hex!("#5CB3FF"),
             "cyan opaque" => hex!("#92C7C7"),
            "cyan or aqua" => hex!("#00FFFF"),
     "dark carnation pink" => hex!("#C12283"),
       "dark forest green" => hex!("#254117"),
          "dark goldenrod" => hex!("#AF7817"),
             "dark orange" => hex!("#F88017"),
             "dark orchid" => hex!("#7D1B7E"),
             "dark salmon" => hex!("#E18B6B"),
          "dark sea green" => hex!("#8BB381"),
         "dark slate blue" => hex!("#2B3856"),
         "dark slate grey" => hex!("#25383C"),
          "dark turquoise" => hex!("#3B9C9C"),
             "dark violet" => hex!("#842DCE"),
            "day sky blue" => hex!("#82CAFF"),
              "deep peach" => hex!("#FFCBA4"),
               "deep pink" => hex!("#F52887"),
           "deep sky blue" => hex!("#3BB9FF"),
              "denim blue" => hex!("#79BAEC"),
         "denim dark blue" => hex!("#151B8D"),
             "desert sand" => hex!("#EDC9AF"),
   "dimorphotheca magenta" => hex!("#E3319D"),
             "dodger blue" => hex!("#1589FF"),
       "dollar bill green" => hex!("#85BB65"),
            "dragon green" => hex!("#6AFB92"),
             "dull purple" => hex!("#7F525D"),
              "earth blue" => hex!("#0000A0"),
                "eggplant" => hex!("#614051"),
           "electric blue" => hex!("#9AFEFF"),
           "emerald green" => hex!("#5FFB17"),
         "fall leaf brown" => hex!("#C8B560"),
              "fern green" => hex!("#667C26"),
             "ferrari red" => hex!("#F70D1A"),
         "fire engine red" => hex!("#F62817"),
               "firebrick" => hex!("#800517"),
           "flamingo pink" => hex!("#F9A7B0"),
            "forest green" => hex!("#4E9258"),
              "frog green" => hex!("#99C68E"),
            "ginger brown" => hex!("#C9BE62"),
        "glacial blue ice" => hex!("#368BC1"),
            "golden brown" => hex!("#EAC117"),
               "goldenrod" => hex!("#EDDA74"),
                 "granite" => hex!("#837E7C"),
                   "grape" => hex!("#5E5A80"),
              "grapefruit" => hex!("#DC381F"),
              "gray cloud" => hex!("#B6B6B4"),
            "gray dolphin" => hex!("#5C5858"),
              "gray goose" => hex!("#D1D0CE"),
               "gray wolf" => hex!("#504A4B"),
                    "gray" => hex!("#736F6E"),
       "grayish turquoise" => hex!("#5E7D7E"),
             "green apple" => hex!("#4CC417"),
             "green onion" => hex!("#6AA121"),
              "green peas" => hex!("#89C35C"),
             "green snake" => hex!("#6CBB3C"),
             "green thumb" => hex!("#B5EAAA"),
            "green yellow" => hex!("#B1FB17"),
                   "green" => hex!("#00FF00"),
           "greenish blue" => hex!("#307D7E"),
                "gunmetal" => hex!("#2C3539"),
        "halloween orange" => hex!("#E66C2C"),
            "harvest gold" => hex!("#EDE275"),
             "hazel green" => hex!("#617C58"),
       "heliotrope purple" => hex!("#D462FF"),
                "hot pink" => hex!("#F660AB"),
       "hummingbird green" => hex!("#7FE817"),
                 "iceberg" => hex!("#56A5EC"),
            "iguana green" => hex!("#9CB071"),
                  "indigo" => hex!("#4B0082"),
                 "iridium" => hex!("#3D3C3A"),
              "jade green" => hex!("#5EFB6E"),
          "jasmine purple" => hex!("#A23BEC"),
              "jeans blue" => hex!("#A0CFEC"),
               "jellyfish" => hex!("#46C7C7"),
                "jet gray" => hex!("#616D7E"),
            "jungle green" => hex!("#347C2C"),
             "kelly green" => hex!("#4CC552"),
              "khaki rose" => hex!("#C5908E"),
                   "khaki" => hex!("#ADA96E"),
              "lapis blue" => hex!("#15317E"),
                "lava red" => hex!("#E42217"),
      "lavender pinocchio" => hex!("#EBDDE2"),
           "lavender blue" => hex!("#E3E4FA"),
              "lawn green" => hex!("#87F717"),
           "lemon chiffon" => hex!("#FFF8C6"),
        "light aquamarine" => hex!("#93FFE8"),
              "light blue" => hex!("#ADDFFF"),
             "light coral" => hex!("#E77471"),
              "light cyan" => hex!("#E0FFFF"),
              "light jade" => hex!("#C3FDB8"),
              "light pink" => hex!("#FAAFBA"),
            "light salmon" => hex!("#F9966B"),
         "light sea green" => hex!("#3EA99F"),
          "light sky blue" => hex!("#82CAFA"),
        "light slate blue" => hex!("#736AFF"),
        "light slate gray" => hex!("#6D7B8D"),
             "light slate" => hex!("#CCFFFF"),
        "light steel blue" => hex!("#728FCE"),
                   "lilac" => hex!("#C8A2C8"),
              "lime green" => hex!("#41A317"),
           "lipstick pink" => hex!("#C48793"),
                "love red" => hex!("#E41B17"),
           "lovely purple" => hex!("#7F38EC"),
     "macaroni and cheese" => hex!("#F2BB66"),
        "macaw blue green" => hex!("#43BFC7"),
                 "magenta" => hex!("#FF00FF"),
                "mahogany" => hex!("#C04000"),
            "mango orange" => hex!("#FF8040"),
             "marble blue" => hex!("#566D7E"),
                  "maroon" => hex!("#810541"),
                   "mauve" => hex!("#E0B0FF"),
       "medium aquamarine" => hex!("#348781"),
     "medium forest green" => hex!("#347235"),
           "medium orchid" => hex!("#B048B5"),
           "medium purple" => hex!("#8467D7"),
        "medium sea green" => hex!("#306754"),
     "medium spring green" => hex!("#348017"),
        "medium turquoise" => hex!("#48CCCD"),
       "medium violet red" => hex!("#CA226B"),
         "metallic silver" => hex!("#BCC6CC"),
           "midnight blue" => hex!("#151B54"),
                "midnight" => hex!("#2B1B17"),
              "milk white" => hex!("#FEFCFF"),
              "mint green" => hex!("#98FF98"),
               "mist blue" => hex!("#646D7E"),
              "misty rose" => hex!("#FBBBB9"),
                "moccasin" => hex!("#827839"),
                   "mocha" => hex!("#493D26"),
                 "mustard" => hex!("#FFDB58"),
               "navy blue" => hex!("#000080"),
            "nebula green" => hex!("#59E817"),
               "neon pink" => hex!("#F535AA"),
                   "night" => hex!("#0C090A"),
    "northern lights blue" => hex!("#78C7C7"),
               "oak brown" => hex!("#806517"),
              "ocean blue" => hex!("#2B65EC"),
                     "oil" => hex!("#3B3131"),
             "orange gold" => hex!("#D4A017"),
           "orange salmon" => hex!("#C47451"),
          "pale blue lily" => hex!("#CFECEC"),
         "pale violet red" => hex!("#D16587"),
           "papaya orange" => hex!("#E56717"),
               "parchment" => hex!("#FFFFC2"),
             "pastel blue" => hex!("#B4CFEC"),
                   "peach" => hex!("#FFE5B4"),
                   "pearl" => hex!("#FDEEF4"),
              "periwinkle" => hex!("#E9CFEC"),
                "pig pink" => hex!("#FDD7E4"),
              "pine green" => hex!("#387C44"),
                "pink bow" => hex!("#C48189"),
          "pink bubblegum" => hex!("#FFDFDD"),
            "pink cupcake" => hex!("#E45E9D"),
              "pink daisy" => hex!("#E799A3"),
           "pink lemonade" => hex!("#E4287C"),
               "pink rose" => hex!("#E7A1B0"),
                    "pink" => hex!("#FAAFBE"),
         "pistachio green" => hex!("#9DC209"),
                "platinum" => hex!("#E5E4E2"),
                "plum pie" => hex!("#7D0541"),
             "plum purple" => hex!("#583759"),
             "plum velvet" => hex!("#7D0552"),
                    "plum" => hex!("#B93B8F"),
             "powder blue" => hex!("#C6DEFF"),
                    "puce" => hex!("#7F5A58"),
          "pumpkin orange" => hex!("#F87217"),
         "purple amethyst" => hex!("#6C2DC7"),
         "purple daffodil" => hex!("#B041FF"),
           "purple dragon" => hex!("#C38EC7"),
           "purple flower" => hex!("#A74AC7"),
             "purple haze" => hex!("#4E387E"),
             "purple iris" => hex!("#571B7E"),
              "purple jam" => hex!("#6A287E"),
           "purple mimosa" => hex!("#9E7BFF"),
          "purple monster" => hex!("#461B7E"),
        "purple sage bush" => hex!("#7A5DC7"),
                  "purple" => hex!("#8E35EF"),
                "red dirt" => hex!("#7F5217"),
                 "red fox" => hex!("#C35817"),
                "red wine" => hex!("#990012"),
                     "red" => hex!("#FF0000"),
          "robin egg blue" => hex!("#BDEDFF"),
              "rogue pink" => hex!("#C12869"),
               "rose gold" => hex!("#ECC5C0"),
                    "rose" => hex!("#E8ADAA"),
              "rosy brown" => hex!("#B38481"),
              "rosy finch" => hex!("#7F4E52"),
              "royal blue" => hex!("#2B60DE"),
     "rubber ducky yellow" => hex!("#FFD801"),
                "ruby red" => hex!("#F62217"),
                    "rust" => hex!("#C36241"),
                 "saffron" => hex!("#FBB917"),
              "sage green" => hex!("#848b79"),
             "salad green" => hex!("#A1C935"),
                    "sand" => hex!("#C2B280"),
               "sandstone" => hex!("#786D5F"),
             "sandy brown" => hex!("#EE9A4D"),
                 "sangria" => hex!("#7E3817"),
           "sapphire blue" => hex!("#2554C7"),
                 "scarlet" => hex!("#FF2400"),
       "school bus yellow" => hex!("#E8A317"),
                "sea blue" => hex!("#C2DFFF"),
               "sea green" => hex!("#4E8975"),
        "sea turtle green" => hex!("#438D80"),
                "seashell" => hex!("#FFF5EE"),
           "seaweed green" => hex!("#437C17"),
                  "sedona" => hex!("#CC6600"),
                   "sepia" => hex!("#7F462C"),
          "shamrock green" => hex!("#347C17"),
         "shocking orange" => hex!("#E55B3C"),
                  "sienna" => hex!("#8A4117"),
               "silk blue" => hex!("#488AC7"),
                "sky blue" => hex!("#6698FF"),
              "slate blue" => hex!("#737CA1"),
              "slate gray" => hex!("#657383"),
             "slime green" => hex!("#BCE954"),
             "smokey gray" => hex!("#726E6D"),
            "spring green" => hex!("#4AA02C"),
              "steel blue" => hex!("#4863A0"),
      "stoplight go green" => hex!("#57E964"),
              "sun yellow" => hex!("#FFE87C"),
          "sunrise orange" => hex!("#E67451"),
               "tan brown" => hex!("#ECE5B6"),
               "tangerine" => hex!("#E78A61"),
                   "taupe" => hex!("#483C32"),
               "tea green" => hex!("#CCFB5D"),
                    "teal" => hex!("#008080"),
                 "thistle" => hex!("#D2B9D3"),
            "tiffany blue" => hex!("#81D8D0"),
            "tiger orange" => hex!("#C88141"),
               "tron blue" => hex!("#7DFDFE"),
              "tulip pink" => hex!("#C25A7C"),
               "turquoise" => hex!("#43C6DB"),
           "tyrian purple" => hex!("#C45AEC"),
           "valentine red" => hex!("#E55451"),
            "vampire gray" => hex!("#565051"),
                 "vanilla" => hex!("#F3E5AB"),
           "velvet maroon" => hex!("#7E354D"),
             "venom green" => hex!("#728C00"),
            "viola purple" => hex!("#7E587E"),
              "violet red" => hex!("#F6358A"),
                  "violet" => hex!("#8D38C9"),
                   "water" => hex!("#EBF4FA"),
         "watermelon pink" => hex!("#FC6C85"),
                   "white" => hex!("#FFFFFF"),
            "windows blue" => hex!("#357EC7"),
         "wisteria purple" => hex!("#C6AEC7"),
                    "wood" => hex!("#966F33"),
            "yellow green" => hex!("#52D017"),
                  "yellow" => hex!("#FFFF00"),
            "zombie green" => hex!("#54C571")
    ];
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Color {
    red: u8,
    green: u8,
    blue: u8,
    alpha: u8,

    name: Option<String>,
}

#[cfg(feature="serde")]
struct ColorVisitor();

impl Color {
    #[inline]
    pub fn rgba(red : u8, green : u8, blue : u8, alpha : u8) -> Self {
        Self {
            red, green, blue, alpha,

            name: None,
        }
    }

    #[inline]
    pub fn rgb(red : u8, green : u8, blue : u8) -> Self {
        Self {
            red, green, blue,

            alpha: u8::MAX,
            name: None,
        }
    }

    #[inline]
    pub fn by_name(name : &str) -> Option<Self> {
        let name = name.to_lowercase();
        if let Some(color) = COLOR_MAP.get(name.as_str()) {
            Some(Self {
                red: color.red,
                green: color.green,
                blue: color.blue,
                alpha: color.alpha,

                name: Some(name.to_string()),
            })
        } else {
            None
        }
    }

    fn parse_hex(dat : &str) -> Result<Self> {
        let dat= dat.trim().trim_start_matches('#');

        match dat.len() {
            3 | 4 | 6 | 8 => {
                let mut out = [0, 0, 0, u8::MAX];

                for (i, c) in dat.char_indices() {
                    if let Some(digit) = c.to_digit(16) {
                        let digit = digit as u8;
                        match dat.len() {
                            3 | 4 => {
                                out[i] = digit | digit << 4;
                            },
                            6 | 8 => {
                                out[i / 2] |= digit << if i % 2 == 0 { 4 } else { 0 }
                            },
                            _ => {},
                        }
                    } else {
                        return Err(Error::BadSyntax);
                    }
                }

                Ok(Self {
                    red : out[0],
                    green : out[1],
                    blue : out[2],
                    alpha : out[3],
                    name: None,
                })
            },
            _ => Err(Error::BadSyntax)
        }
    }

    pub fn to_string(&self) -> String {
        if let Some(name) = &self.name {
            name.clone()
        } else {
            if self.alpha != u8::MAX {
                format!("#{:02X}{:02X}{:02X}", self.red, self.green, self.blue)
            } else {
                format!("#{:02X}{:02X}{:02X}{:02X}", self.red, self.green, self.blue, self.alpha)
            }
        }
    }
}

impl TryFrom<&str> for Color {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self> {
        if let Some(color) = Color::by_name(value) {
            Ok(color)
        } else if value.starts_with('#') {
            Color::parse_hex(value)
        } else {
            let value = value.trim();
            let mut function_name = None;
            let mut args_begin = None;
            let mut args = None;

            for (i, c) in value.char_indices() {
                if let None = function_name {
                    if c.is_alphanumeric() {
                        if i == 0 && c.is_numeric() {
                            return Err(Error::BadSyntax);
                        }
                    } else if c.is_whitespace() {
                        function_name = value.get(0..i);
                    } else if c == '(' {
                        function_name = value.get(0..i);
                        args_begin = Some(i);
                    } else {
                        return Err(Error::BadSyntax);
                    }
                } else {
                    if let Some(begin) = args_begin {
                        if let None = args {
                            if c == ')' {
                                if let Some(arguments) = value.get(begin+1..i) {
                                    let mut arguments_list = LinkedList::new();

                                    for i in arguments.split(',') {
                                        if let Ok(arg) = i.trim().parse::<u8>() {
                                            arguments_list.push_back(arg);
                                        } else {
                                            return Err(Error::BadSyntax);
                                        }
                                    }

                                    args = Some(arguments_list);
                                }
                            }
                        } else {
                            if !c.is_whitespace() {
                                return Err(Error::BadSyntax);
                            }
                        }
                    } else {
                        if c == '(' {
                            args_begin = Some(i);
                        } else if !c.is_whitespace() {
                            return Err(Error::BadSyntax);
                        }
                    }
                }
            }

            if let Some(function_name) = function_name {
                if let Some(mut args) = args {
                    use std::cmp::Ordering;
                    let args_number = args.len();
                    match function_name {
                        "rgb" => match args_number.cmp(&3) {
                            Ordering::Greater => Err(TooManyArguments(args_number, 3)),
                            Ordering::Less => Err(NotEnoughArguments(args_number, 3)),
                            _ => {
                                Ok(Color::rgb(
                                    args.pop_front().unwrap(),
                                    args.pop_front().unwrap(),
                                    args.pop_front().unwrap()
                                ))
                            }
                        },
                        "rgba" => match args_number.cmp(&4) {
                            Ordering::Greater => Err(TooManyArguments(args_number, 4)),
                            Ordering::Less => Err(NotEnoughArguments(args_number, 4)),
                            _ => {
                                Ok(Color::rgba(
                                    args.pop_front().unwrap(),
                                    args.pop_front().unwrap(),
                                    args.pop_front().unwrap(),
                                    args.pop_front().unwrap()
                                ))
                            }
                        },
                        _ => {
                            Err(Error::UnknownFunction(function_name.to_string()))
                        }
                    }
                } else {
                    Err(Error::BadSyntax)
                }
            } else {
                Err(Error::BadSyntax)
            }
        }
    }
}

impl Default for Color {
    fn default() -> Self {
        Color::rgb(0, 0, 0)
    }
}

#[cfg(feature="serde")]
impl Serialize for Color {
    fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error> where S: Serializer {
        serializer.serialize_str(self.to_string().as_str())
    }
}

#[cfg(feature="serde")]
impl<'de> Visitor<'de> for ColorVisitor {
    type Value = Color;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(formatter, "a color value (rgb(23, 31, 15), rgba(18,39,41,52), #04F32C, #067F31ED...)")
    }

    fn visit_str<E>(self, v: &str) -> std::result::Result<Self::Value, E> where E: serde::de::Error {
        match Color::try_from(v) {
            Ok(v) => Ok(v),
            Err(_) => Err(serde::de::Error::invalid_value(
                serde::de::Unexpected::Str(v),
                &self
            ))
        }
    }
}

#[cfg(feature="serde")]
impl<'de> Deserialize<'de> for Color {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_str(ColorVisitor())
    }
}

#[cfg(feature="schemars")]
fn generate_enum_from_map<T>(from : &HashMap<&str, T>) -> Vec<serde_json::value::Value> {
    from.keys().map(|v| {
        serde_json::value::Value::String(v.to_string())
    }).collect()
}

#[cfg(feature="schemars")]
impl JsonSchema for Color {
    fn schema_name() -> String {
        "Color".to_string()
    }

    fn json_schema(_gen: &mut schemars::gen::SchemaGenerator) -> schemars::schema::Schema {
        Schema::Object(SchemaObject {
            metadata : None,
            instance_type : Some(schemars::schema::SingleOrVec::Single(Box::new(InstanceType::String))),
            format: None,
            enum_values: Some(generate_enum_from_map(&COLOR_MAP)),
            const_value: None,
            subschemas: None,
            number: None,
            string: Some(Box::new(StringValidation {
                max_length: None,
                min_length: None,
                pattern: Some("^#[[:xdigit:]]{3}[[:xdigit:]]?$|#[[:xdigit:]]{6}(?:[[:xdigit:]]{2})?$|rgb\\((?: *0*(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]){1} *,){2}(?: *0*(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]){1} *)\\)$|rgba\\((?: *0*(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]){1} *,){3}(?: *0*(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]){1} *)\\)$".to_string())
            })),
            array: None,
            object: None,
            reference: None,
            extensions: Map::new(),
        })
    }
}