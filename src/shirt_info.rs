use std::borrow::Cow;
use std::cmp::Ordering;
use std::collections::{HashMap, LinkedList};
use crate::error::Result;
use crate::format::{CanPurchase, ShirtSize, Gender, Format, Person};
use serde::Serialize;

#[derive(Serialize, Eq, PartialEq)]
pub struct ShirtInfo<'a> {
    #[serde(rename="Prénom")]
    first_name: &'a str,
    #[serde(rename="Nom")]
    last_name: &'a str,
    #[serde(rename="Rôle")]
    role: &'a str,
    #[serde(rename="Surnom")]
    nickname: Cow<'a, str>,
    #[serde(rename="Taille")]
    size: ShirtSize,
    #[serde(rename="Sexe")]
    gender: Gender,
    #[serde(rename="Club")]
    club: &'a str,
    #[serde(rename="Quantité")]
    quantity: usize,
    #[serde(rename="Couleur")]
    color: &'a str,
    #[serde(rename="Couleur du texte")]
    text_color: &'a str,
    #[serde(rename="Peut le payer")]
    can_purchase: CanPurchase,
}

impl<'a> ShirtInfo<'a> {
    fn get_role(format : &'a Format, member : &'a Person, club_id : &str, role_id : &'a str) -> (&'a str, CanPurchase) {
        let mut role_alias = None;
        let mut can_purchase = CanPurchase::Undef;

        let role_info = format.roles.get(role_id);

        // Récupération de l’alias du rôle spécifique au membre
        if let Some(shirt_info) = &member.shirt_info {
            if let Some(shirt_info) = shirt_info.clubs.get(club_id) {
                can_purchase = shirt_info.can_purchase;
                if let Some(alias) = &shirt_info.role_alias {
                    role_alias = Some(alias.as_str());
                }
            }
        }

        // Récupération de l’information du rôle permettant de savoir si l’habit est
        // pris en charge ou non par le BDE.
        if let Some(role_info) = role_info {
            if role_info.official {
                can_purchase = CanPurchase::Given;
            }
        }

        let ret = if let Some(ret) = role_alias {
            ret
        } else {
            // Récupération du nom du rôle donné par son identifiant
            if let Some(role_info) = role_info {
                if let Some(alias) = &role_info.alias {
                    alias.as_str()
                } else {
                    match &member.gender {
                        Gender::Men => role_info.name.as_str(),
                        Gender::Women => if let Some(variant) = &role_info.variant {
                            variant.as_str()
                        } else {
                            role_info.name.as_str()
                        }
                    }
                }
            } else {
                role_id
            }
        };

        (ret, can_purchase)
    }

    fn get_nickname(member : &'a Person, club_id : &str) -> Cow<'a, str> {
        let mut nickname = None;

        if let Some(shirt_info) = &member.shirt_info {
            if let Some(shirt_info) = shirt_info.clubs.get(club_id) {
                nickname = Some(shirt_info.nickname.as_str());
            }
        }

        if let Some(nickname) = nickname {
            Cow::Borrowed(nickname)
        } else {
            Cow::Owned(format!("{} {}", member.first_name, member.last_name))
        }
    }

    pub fn extract_from(format : &'a Format) -> Result<Vec<Self>> {
        use crate::error::Error;

        let mut list = LinkedList::new();

        for (club_id, club_info) in &format.clubs.list {
            let mut members = HashMap::new();

            for member_info in &club_info.members {
                members.insert(member_info.name.as_str(), member_info.role.as_str());
            }

            for section_info in club_info.sections.list.values() {
                for member_info in section_info {
                    members.entry(member_info.name.as_str()).or_insert_with(|| member_info.role.as_str());
                }
            }

            for (name, role_id) in members {
                let member = if let Some(m) = format.people.get(name) {
                    m
                } else {
                    return Err(Error::UnknownPerson(name.to_string()));
                };

                if let Some(shirt_info) = &member.shirt_info {
                    let (role, can_purchase) = Self::get_role(format, member, club_id, role_id);
                    let nickname = Self::get_nickname(member, club_id);

                    let to_add = ShirtInfo {
                        first_name: member.first_name.as_str(),
                        last_name: member.last_name.as_str(),
                        role, nickname,
                        size: shirt_info.size,
                        gender: member.gender,
                        club: club_info.name.as_str(),
                        quantity: 1,
                        color: club_info.shirt_color.as_str(),
                        text_color: club_info.shirt_serigraph_color.as_str(),
                        can_purchase: if club_info.give_all_shirts {
                            CanPurchase::Given
                        } else {
                            can_purchase
                        }
                    };

                    list.push_back(to_add);
                }
            }

            for extra_shirt in &club_info.extra_shirts {
                let to_add = ShirtInfo {
                    first_name: extra_shirt.customer_first_name.as_str(),
                    last_name: extra_shirt.customer_last_name.as_str(),
                    role: extra_shirt.role.as_str(),
                    nickname: Cow::Borrowed(extra_shirt.name.as_str()),
                    size: extra_shirt.size,
                    gender: extra_shirt.gender,
                    club: club_info.name.as_str(),
                    quantity: extra_shirt.quantity,
                    color: club_info.shirt_color.as_str(),
                    text_color: club_info.shirt_serigraph_color.as_str(),
                    can_purchase: if club_info.give_all_shirts {
                        CanPurchase::Given
                    } else {
                        CanPurchase::Can
                    },
                };

                list.push_back(to_add);
            }
        }

        let mut ret = Vec::with_capacity(list.len());

        while let Some(current) = list.pop_front() {
            ret.push(current);
        }

        Ok(ret)
    }
}

impl<'a> PartialOrd for ShirtInfo<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        use std::cmp::Ordering::*;

        match Ord::cmp(self, other) {
            Greater => Some(Greater),
            Less => Some(Less),
            Equal => None,
        }
    }
}

impl<'a> Ord for ShirtInfo<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        use std::cmp::Ordering::*;

        match Ord::cmp(self.club, other.club) {
            Greater => Greater,
            Less => Less,
            Equal => {
                if self.nickname.is_empty() ^ other.nickname.is_empty() {
                    if self.nickname.is_empty() {
                        Greater
                    } else {
                        Less
                    }
                } else {
                    match std::cmp::Ord::cmp(self.first_name, other.first_name) {
                        Greater => Greater,
                        Less => Less,
                        Equal => {
                            match std::cmp::Ord::cmp(self.last_name, other.last_name) {
                                Greater => Greater,
                                Less => Less,
                                Equal => Equal
                            }
                        }
                    }
                }
            }
        }
    }
}