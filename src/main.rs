mod format;
mod error;
mod cards;
mod shirt_info;
mod path_reader;

use std::{collections::HashSet, process::ExitCode};
use std::io::Write;
use std::path::PathBuf;
use clap::{Parser, Subcommand};
use serde::{ Deserialize, Serialize };
use crate::shirt_info::ShirtInfo;

#[cfg(feature="gen-schema")]
use clap::ValueEnum;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(subcommand)]
    command : Commands,

    /// Path to the configuration file
    ///
    /// If this file is a `toml` file, it must follow the same format than the following example :
    /// ```toml
    /// [people.john_doe]
    /// picture="john_doe.jpg"
    /// first-name="John"
    /// last-name="Doe"
    /// email="john.doe@example.com"
    /// phone="+1-555-753-6236"
    /// gender="H"
    ///
    /// [roles.president]
    /// name="Président"
    /// variant="Présidente"
    /// alias="PreZZ"
    /// official=true
    ///
    /// [clubs.foo]
    /// name="FOO"
    /// logo="foo.svg"
    /// primary-color="#1e191a"
    /// secondary-color="#00579d"
    /// on-primary-color="#f1f1f1"
    /// members=[
    ///     { "name"="john_doe", "role"="president" }
    /// ]
    /// ```
    ///
    /// In the case of the path points to a directory, the file named `info.toml` is
    /// first parsed then files named `<category>.toml` are parsed and finally files
    /// with path `<category>/<key>.toml` are parsed.
    input_file: Option<PathBuf>,

    /// Path to the output file, the result is written in stdout if not specified
    ///
    /// This file must be at the right place to use resources access specified in
    /// the template file
    #[clap(short='o', long)]
    output_file: Option<PathBuf>,

    #[clap(short='s', long)]
    /// Show statistics about the current configuration file
    statistics: bool,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Generate face book
    Gen(GenArgs),

    /// Generate shirt informations
    GenShirtInfo,

    /// List all clubs with its name
    ListClubs(ListClubsArgs),

    #[cfg(feature="gen-schema")]
    /// Generate schema files
    GenSchema(GenSchemaArgs),
}

#[derive(Parser, Debug)]
struct GenArgs {
    #[clap(short='t', long, default_value=env!("DEFAULT_TEMPLATE_PATH"))]
    /// Path to the template directory
    template_path: PathBuf,

    #[clap(short='c', long)]
    /// Show private informations about members set in the configuration file
    show_credentials: bool,

    #[clap(short='s', long)]
    /// Select the clubs to render. All clubs are rendered when empty
    select_clubs: Vec<String>,
}

#[derive(Parser, Debug)]
pub(crate) struct ListClubsArgs {
    #[clap(short='n', long)]
    /// Print the name of the club
    print_name: bool,

    #[clap(short='i', long)]
    /// Print the id of the club
    print_id: bool,
}

#[derive(Parser, Debug)]
#[cfg(feature="gen-schema")]
pub(crate) struct GenSchemaArgs {
    file_level : SchemaLevel
}

#[derive(ValueEnum, Clone, Debug)]
#[cfg(feature="gen-schema")]
pub(crate) enum SchemaLevel {
    Root,
    Person,
    Club
}

#[derive(Deserialize, Serialize)]
struct MemberList<'a> {
    #[serde(rename="First name")]
    first_name : &'a str,
    #[serde(rename="Last name")]
    last_name: &'a str,
    #[serde(rename="eMail address")]
    email: &'a str,
    #[serde(rename="Phone number")]
    phone: &'a str,
    #[serde(rename="Inner-club count")]
    club_count: usize,
    #[serde(rename="Clubs")]
    clubs: String,
}

impl ListClubsArgs {
    pub fn count_fields(&self) -> usize {
        let mut ret = 0;

        if self.print_name { ret += 1; }
        if self.print_id { ret += 1; }

        ret
    }
}

fn into_hashset(from : &[String]) -> HashSet<&str> {
    let mut ret = HashSet::with_capacity(from.len());

    for i in from {
        ret.insert(i.as_str());
    }

    ret.shrink_to_fit();

    ret
}

fn generate_face_book(out: &mut Box<dyn std::io::Write>, data : &cards::Page, args : &GenArgs) -> error::Result<()> {
    let mut template_path = args.template_path.clone();
    template_path.push("**/*.html");
    let template = tera::Tera::new(template_path.as_os_str().to_str().unwrap())?;

    let mut ctx = tera::Context::from_serialize(data)?;
    ctx.insert("show_credentials", &args.show_credentials);

    let generated = template.render("index.html", &ctx)?;

    out.write_all(generated.as_bytes())?;

    Ok(())
}

fn generate_shirt_info_table(out: &mut Box<dyn std::io::Write>, data : Vec<ShirtInfo>) {
    use csv::Writer;

    let mut writer = Writer::from_writer(out);

    for row in data {
        writer.serialize(row).unwrap();
    }

    writer.flush().unwrap();
}

fn array_matrix_to_string(matrix : Vec<Vec<&str>>, total_size : usize) -> String {
    let mut data = String::with_capacity(total_size);

    for line in matrix {
        let mut idx = 0;
        let line_length = line.len();

        for entry in line {
            idx += 1;

            data.push_str(entry);

            data.push(if idx == line_length {
                '\n'
            } else {
                ' '
            });
        }
    }

    data
}

fn app() -> error::Result<()> {
    let args : Args = Args::parse();

    let data = if let Some(input) = args.input_file {
        Some(format::load(input.as_path())?)
    } else { None };

    let mut out : Box<dyn std::io::Write> = if let Some(p) = &args.output_file {
        Box::new(std::fs::File::create(p.as_path())?)
    } else {
        Box::new(std::io::stdout())
    };

    match &args.command {
        Commands::Gen(args) => {
            if let Some(data) = data {
                let cards = data.generate_cards(into_hashset(args.select_clubs.as_slice()));
    
                generate_face_book(&mut out, &cards, args)?;
            } else {
                return Err(error::Error::NoInputFile);
            }
        },
        Commands::GenShirtInfo => {
            if let Some(data) = data {
                let mut shirt_info = data.generate_shirt_info()?;

                shirt_info.sort();

                generate_shirt_info_table(&mut out, shirt_info);
            } else {
                return Err(error::Error::NoInputFile);
            }
        },
        Commands::ListClubs(args) => {
            if let Some(data) = data {
                if let Some((lines, total_size)) = data.generate_club_list(args) {
                    let data = array_matrix_to_string(lines, total_size);

                    out.write_all(data.as_bytes())?;
                } else {}
            } else {
                return Err(error::Error::NoInputFile);
            }
        },
        #[cfg(feature="gen-schema")]
        Commands::GenSchema(args) => {
            let mut gen = schemars::gen::SchemaGenerator::default();

            let root = match args.file_level {
                SchemaLevel::Root => {
                    gen.root_schema_for::<format::Format>()
                },
                SchemaLevel::Club => {
                    gen.root_schema_for::<format::Club>()
                },
                SchemaLevel::Person => {
                    gen.root_schema_for::<format::Person>()
                }
            };

            serde_json::to_writer_pretty(out, &root).unwrap();
        }
    }

    Ok(())
}

fn main() -> ExitCode {
    tracing::subscriber::set_global_default(
        tracing_subscriber::FmtSubscriber::builder()
            .with_max_level(tracing::Level::DEBUG)
            .finish()
    ).unwrap();

    if let Err(e) = app() {
        tracing::error!("{}", e);
        
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}

#[cfg(test)]
mod test {
    use crate::array_matrix_to_string;

    #[test]
    fn matrix_to_string() {
        let matrix = vec![
            vec!["Lorem", "ipsum", "dolor"],
            vec!["Sit", "amet"]
        ];
        let size = 27;

        let out = "Lorem ipsum dolor\nSit amet\n";

        assert_eq!(array_matrix_to_string(matrix, size), out);
    }
}